import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sudong.dao.UserMapper;
import com.sudong.pojo.Address;
import com.sudong.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class MybatisTest {
    SqlSessionFactory sqlSessionFactory = null;
    @Before
    public void init() throws IOException {
        String resource = "mybatis_config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }
    @Test
    public void testSelect()  {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        User user = userMapper.find(3);

        System.out.println(user);

        sqlSession.close();

    }
    @Test
    public void insertUser() throws IOException {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        User user = new User();
        user.setName("hzz发发发发z");
        user.setPassword("aaaaaaa");
        user.setPhone("17851185357");
        user.setEmail("757682793@qq.com");
        user.setGender("女");
        user.setModified_time(new Timestamp(System.currentTimeMillis()));
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        userMapper.insertUser(user);
        sqlSession.commit();
        sqlSession.close();
    }
    @Test
    public void deleteUser() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        userMapper.deleteUser(4);
        sqlSession.commit();
        sqlSession.close();
    }
    @Test
    public void updateUser(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        User user = new User();
        user.setId(2);
        user.setName("周杰伦");
        userMapper.updateUser(user);
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void selectAll(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        List<User> list = sqlSession.selectList("usermapper.selectAllUser");
        for (User user: list) {
            System.out.println(user);
        }
        sqlSession.close();
    }
    @Test
    public void testSelectAllInterface(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        List<User> users = userMapper.selectAllUser();
        for (User u: users) {
            System.out.println(u);
        }
        sqlSession.close();
    }
    @Test
    public void testSelectIf(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        User user = new User();

        user.setId(3);
        List<User> users = userMapper.selectIF(user);
        for (User u :
                users) {
            System.out.println(u);
        }
        sqlSession.close();
    }
    @Test
    public void updateSet(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        User user = new User();
        user.setId(2);
        user.setName("hhh");
        userMapper.updateSet(user);
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void deleteForeach(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        List<Integer> ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        userMapper.deleteForeach(ids);
        sqlSession.commit();
        sqlSession.close();
    }
    @Test
    public void selectChoose(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        User user = new User();

        List<User> users = userMapper.selectChoose(user);
        for (User u :
                users) {
            System.out.println(u);
        }
        sqlSession.close();
    }
    @Test
    public void findUserAndFamily(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        Page<Object> pageHelper = PageHelper.startPage(2,2);
        List<User> users =  userMapper.findUserAndFamily();
        for (User u :
                users) {
            System.out.println(u);

        }
        sqlSession.close();
    }
    @Test
    public void findUserAndAddress(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        Page<Object> pageHelper = PageHelper.startPage(1,3);
        List<User> users =  userMapper.findUserAndAddress();
        for (User u : users) {
            System.out.println(u);
        }
        sqlSession.close();
    }
    @Test
    public void getAddressByUserId(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        List<Address> addresses = userMapper.getAddressByUserId(1);
        for (Address a :
                addresses) {
            System.out.println(a);
        }
        sqlSession.close();
    }
}
