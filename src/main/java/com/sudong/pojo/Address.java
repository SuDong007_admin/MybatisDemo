package com.sudong.pojo;

import java.io.Serializable;

public class Address implements Serializable {
    private int aid;
    private int uid;
    private String recv_name;
    private String recv_district;
    private String recv_addr;

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getRecv_name() {
        return recv_name;
    }

    public void setRecv_name(String recv_name) {
        this.recv_name = recv_name;
    }

    public String getRecv_district() {
        return recv_district;
    }

    public void setRecv_district(String recv_district) {
        this.recv_district = recv_district;
    }

    public String getRecv_addr() {
        return recv_addr;
    }

    public void setRecv_addr(String recv_addr) {
        this.recv_addr = recv_addr;
    }

    @Override
    public String toString() {
        return "Address{" +
                "aid=" + aid +
                ", uid=" + uid +
                ", recv_name='" + recv_name + '\'' +
                ", recv_district='" + recv_district + '\'' +
                ", recv_addr='" + recv_addr + '\'' +
                '}';
    }
}
