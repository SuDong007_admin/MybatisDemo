package com.sudong.pojo;

import java.io.Serializable;

public class Family implements Serializable {
    private int fid;
    private int number;
    private String telephone;
    private int uid;

    public int getFid() {
        return fid;
    }

    public void setFid(int fid) {
        this.fid = fid;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "Family{" +
                "fid=" + fid +
                ", number=" + number +
                ", telephone='" + telephone + '\'' +
                ", uid=" + uid +
                '}';
    }
}
