package com.sudong.dao;

import com.sudong.pojo.Address;
import com.sudong.pojo.User;

import java.util.List;

public interface UserMapper {
    public List<User> selectAllUser();
    public User find(int i);
    public void insertUser(User user);
    public void deleteUser(int i);
    public void updateUser(User user);
    public List<User> selectIF(User user);
    public void updateSet(User user);
    public void deleteForeach(List<Integer> ids);
    public List<User> selectChoose(User user);
    public List<User> findUserAndFamily();
    public List<User> findUserAndAddress();
    public List<Address> getAddressByUserId(int id);
}
